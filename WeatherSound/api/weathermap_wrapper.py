import requests
import os
from datetime import date, datetime


BASE_URL = "https://api.openweathermap.org/data/2.5/"
API_KEY = os.getenv('API_KEY')


def get_current_weather(lat: float, lon: float):
    response = requests.get(
        BASE_URL+"weather",
        params={
            'lat': lat,
            'lon': lon,
            'appid': API_KEY,
            'units': 'metric',
            'lang': 'fr'
        }
    )
    if response.ok:
        data = response.json()
        current_weather = {
            "city": data["name"],
            "current_temperature": round(data.get("main").get("temp"), 1),
            "temperature_min": round(data.get("main").get("temp_min"), 1),
            "temperature_max": round(data.get("main").get("temp_max"), 1),
            "feels_like": round(data.get("main").get("feels_like"), 1),
            "weather_condition": data.get("weather")[0].get("main"),
            "wind_speed": round(data.get("wind").get("speed"), 1),
            "cloud": data.get("clouds").get("all"),
            "date": datetime.utcfromtimestamp(data["dt"]).strftime('%d-%m-%Y %H:%M:%S')
        }
        # verify if rain attribute exists then set rain volume for the last 1 hour
        if 'rain' in data:
            current_weather["rain"] = data["rain"]["1h"]
        else:
            current_weather["rain"] = 0
        return current_weather
    return None


def get_daily_weather(latitude: float, longitude: float, date_stamp: date):
    """
    Return the daily weather forecast
    for a given date
    """
    response = requests.get(
        BASE_URL + "onecall",
        params={
            "lat": latitude,
            "lon": longitude,
            "appid": API_KEY,
            "exlude": "current, minutely, hourly, alerts",
            "units": "metric",
            "lang": "fr"
        })

    if response.ok:
        data = response.json()
        daily_weather = {}
        for i in range(7):
            """
                1-Loop into the global data from the weather API
                2-Compare the input date_request with dates provided by the weather API
                3-When the dates match, collect necessary weather data related
                4-Gather weather data for Morning, Evening and Night
                5-Save weather data in daily_weather JSON  file
            """
            timestamp = data.get("daily")[i].get("dt")
            data_date = datetime.fromtimestamp(timestamp).date()

            if date_stamp == data_date:
                # Weather forecast for the morning
                morning_forecast_weather = {
                    "current_temperature": data.get("daily")[i].get("temp").get("morn"),
                    "feels_like_temperature": data.get("daily")[i].get("feels_like").get("morn")
                }
                # Weather forecast for the afternoon
                afternoon_forecast_weather = {
                    "current_temperature": data.get("daily")[i].get("temp").get("day"),
                    "feels_like_temperature": data.get("daily")[i].get("feels_like").get("day")
                }
                # Weather forecast for the evening
                evening_forecast_weather = {
                    "current_temperature": data.get("daily")[i].get("temp").get("eve"),
                    "feels_like_temperature": data.get("daily")[i].get("feels_like").get("eve")
                }
                # Weather forecast for the night
                night_forecast_weather = {
                    "current_temperature": data.get("daily")[i].get("temp").get("night"),
                    "feels_like_temperature": data.get("daily")[i].get("feels_like").get("night")
                }
                # Weather forecast for the day
                daily_weather = {
                    "temperature_min": data.get("daily")[i].get("temp").get("min"),
                    "temperature_max": data.get("daily")[i].get("temp").get("max"),
                    "weather_condition": data.get("daily")[i].get("weather")[0].get("main"),
                    "wind_speed": data.get("daily")[i].get("wind_speed"),
                    "cloud": data.get("daily")[i].get("clouds"),
                    "rain": data.get("daily")[i].get("rain"),
                    "date": data_date,
                    "morning": morning_forecast_weather,
                    "afternoon": afternoon_forecast_weather,
                    "evening": evening_forecast_weather,
                    "night": night_forecast_weather,
                }
                # verify if rain attribute exists then set rain volume for the last 1 hour

                if 'rain' in data:
                    daily_weather["rain"] = data.get("daily")[i].get("rain")
                else:
                    daily_weather["rain"] = 0
        return daily_weather
    return None


def get_weekly_weather(latitude: float, longitude: float):
    """
    Return weather forecast for one week
    Starting from the query
    """
    print("Glouglou")
    # Get the global data from the weather API
    response = requests.get(
        BASE_URL + "onecall",
        params={
            "lat": latitude,
            "lon": longitude,
            "appid": API_KEY,
            "exlude": "current, minutely, hourly, alerts",
            "units": "metric",
            "lang": "fr"
        })
    if response.ok:
        data = response.json()
        data.get("daily")
        weekly_weather = []
        for i in range(7):
            weather = {
                "current_temperature": data.get("daily")[i].get("temp").get("day"),
                "feels_like_temperature": data.get("daily")[i].get("feels_like").get("day"),
                "temperature_min": data.get("daily")[i].get("temp").get("min"),
                "temperature_max": data.get("daily")[i].get("temp").get("max"),
                "weather_condition": data.get("daily")[i].get("weather")[0].get("main"),
                "wind_speed": data.get("daily")[i].get("wind_speed"),
                "cloud": data.get("daily")[i].get("clouds"),
                "rain": data.get("daily")[i].get("rain"),
                "date": datetime.fromtimestamp(data.get("daily")[i].get("dt")).date().isoformat()
            }
            # verify if rain attribute exists then set rain volume for the last 1 hour
            if 'rain' in data:
                weather["rain"] = data.get("daily")[i].get("rain")
            else:
                weather["rain"] = 0

            weekly_weather.append(weather)

        return weekly_weather
    return None
