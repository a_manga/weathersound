from rest_framework import serializers
from .models import City, Country


class GetCurrentWeatherSerializer(serializers.Serializer):
    latitude = serializers.FloatField(required=True)
    longitude = serializers.FloatField(required=True)


class CurrentWeatherSerializer(serializers.Serializer):
    city = serializers.CharField(max_length=30)
    current_temp = serializers.FloatField()
    temp_min = serializers.FloatField()
    temp_max = serializers.FloatField()
    feels_like = serializers.FloatField()
    weather_condition = serializers.CharField(max_length=20)
    wind_speed = serializers.FloatField()
    cloud = serializers.IntegerField()
    rain = serializers.IntegerField(default=0, required=False)
    date = serializers.DateField(format="%d-%m-%Y %H:%M:%S")


class GetDailyWeatherSerializer(serializers.Serializer):
    latitude = serializers.FloatField(min_value=-90, max_value=90)
    longitude = serializers.FloatField(min_value=-180, max_value=180)
    date = serializers.DateField(format=['iso-8601'])


class DayPeriodWeatherSerializer(serializers.Serializer):
    current_temperature = serializers.FloatField()
    feels_like_temperature = serializers.FloatField()


class DailyWeatherSerializer(serializers.Serializer):
    temp_min = serializers.FloatField()
    temp_max = serializers.FloatField()
    feels_like = serializers.FloatField()
    weather_condition = serializers.CharField(max_length=20)
    wind_speed = serializers.FloatField()
    cloud = serializers.IntegerField()
    rain = serializers.IntegerField(default=0, required=False)
    date = serializers.DateField(format=['iso-8601'])
    morning = DayPeriodWeatherSerializer()
    afternoon = DayPeriodWeatherSerializer()
    evening = DayPeriodWeatherSerializer()
    night = DayPeriodWeatherSerializer()


class GetCitySerializer(serializers.Serializer):
    city_query = serializers.CharField()


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = "__all__"


class CitySerializer(serializers.ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = City
        fields = ["geoname_id", "name", "country", "longitude", "latitude"]


class WeekWeatherSerializer(serializers.Serializer):
    data = CurrentWeatherSerializer(many=True)
