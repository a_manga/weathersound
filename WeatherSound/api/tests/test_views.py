# https://www.dev2qa.com/how-to-use-django-test-client-to-test-views/
import unittest
from django.test import Client
from datetime import datetime

class ViewsTestCase(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def test_current_weather(self):
        """The current weather retrieved properly"""
        response = self.client.get('/api/v1/current_weather', {'longitude': 48.8566, 'latitude': 2.3522})
        self.assertEqual(response.status_code, 200)

    def test_daily_weather(self):
        """The daily weather retrieved properly"""
        date_stamp = datetime.now().strftime('%Y-%m-%d')
        response = self.client.get('/api/v1/daily_weather', {'longitude': 48.8566, 'latitude': 2.3522, 'date': date_stamp})
        self.assertEqual(response.status_code, 200)

    def test_weekly_weather(self):
        """The weekly weather retrieved properly"""
        response = self.client.get('/api/v1/weekly_weather', {'longitude': 48.8566, 'latitude': 2.3522})
        self.assertEqual(response.status_code, 200)

    def test_cities(self):
        """The city retrieved properly"""
        response = self.client.get('/api/v1/cities', {'city_query': "Plouzané"})
        self.assertEqual(response.status_code, 200)

    def test_cities_coordinates(self):
        """The Plouzané city coordinates retrieved properly"""
        response = self.client.get('/api/v1/cities', {'city_query': "Plouzané"})
        self.assertEqual(response.json()[0].get('longitude'), -4.61805)
        self.assertEqual(response.json()[0].get('latitude'), 48.38131)
