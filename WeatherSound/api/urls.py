"""WeatherSound URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
import os


schema_view = get_schema_view(
    openapi.Info(
        title="WeatherSound API",
        default_version=os.getenv('VERSION', default="1"),
        description="API de l'application WeatherSound permettant de récuperer la météo d'une localité.",
        license=openapi.License(name="MIT License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny, ),
)

urlpatterns = [
    path('', schema_view.with_ui('swagger', cache_timeout=0), name="schema_swagger_ui"),
    path('redoc', schema_view.with_ui('redoc', cache_timeout=0), name='schema_redoc'),
    path('current_weather', views.current_weather),
    path('daily_weather', views.daily_weather),
    path('weekly_weather', views.weekly_weather),
    path('cities', views.cities)
]
